
Flag Classes
------------

This module wraps flagged items with useful CSS classes.

These classes are updated dynamically, through JavaScript, when AJAX flag links are used.

Views styled as tables and lists too are affected.


Status
------

This is the first version of this module, and as such isn't very efficient.


Homepage
--------

http://drupal.org/project/flag_classes
