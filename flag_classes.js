(function ($) {
  var debug = false;

  Drupal.behaviors.flagClasses = {
    attach: function (context, settings) {
      // Update the CSS classes of flagged entities after their flag link is clicked.
      $('body', context).once('flagClasses', function() {
        var flagStatuses = ['flagged-', 'unflagged-'],
          flagSelfSuffix = '-self';

        $(document, context).bind('flagGlobalBeforeLinkUpdate', function(event, data) {
          var flagged = data.flagStatus == 'flagged',
            flagClass = data.entityType + '-' + data.flagName + '-' + data.contentId.replace(/_/g, '-'),
            $flagParent = $(data.link).parents('.flaggable');

          // Check if parent exists.
          if ($flagParent.length) {
            var statusActive = flagged ? flagStatuses[1] : flagStatuses[0],
              statusInactive = flagged ? flagStatuses[0] : flagStatuses[1];

            $flagParent.removeClass(statusActive + flagClass);
            $flagParent.addClass(statusInactive + flagClass);

            if ($flagParent.hasClass(statusActive + flagClass + flagSelfSuffix)) {
              $flagParent.removeClass(statusActive + flagClass + flagSelfSuffix);
              $flagParent.addClass(statusInactive + flagClass + flagSelfSuffix);
            }
          }
          else {
            if (debug) {
              var error = Drupal.t('Flag parent does not exist for flag of type: @flagName on @entityType with id: @entityId', {'@flagName': data.flagName, '@entityType' : data.entityType, '@entityId': data.contentId});

              console.log(error);
            }
          }
        });
      });
    }
  };
})(jQuery);
